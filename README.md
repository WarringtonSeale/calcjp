# calcjp

A python calculator made for my 'Jezyki Programowania' course, 2019

/calcjp/dist/CALCJP_TMP contains a packaged app - runs on win10x64 ( calcjp.exe in cmd)

Final version as of 6/6/2019

# Dependencies

kivy 1.11.0   // gui

#Todo
[* ] Make WeclcomeScreen

[* ] Make CalcScreen


[* ] Enable number buttons


[* ] Enable operation buttons


[* ] Make it count (hehe)


[?* ] Unallow multiple decimal dots / app will just crash - good enough


[* ] Enable AC button

[* ] Enable C button



[* ] format the result nicely


[?* ] deal with division by 0   / app will just crash - good enough
