import kivy
from kivy.app  import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty
from kivy.config import Config

kivy.require('1.11.0')
Config.set('graphics', 'width', '300')
Config.set('graphics', 'height', '480')

class CalcjpApp(App):
    def build(self):
        MakeUI = Builder.load_file('calcjp.kv')
        return MakeUI


class MainScreen(Screen):
    pass

class CalcScreen(Screen):
    dispVar = ObjectProperty()
    Var1 = ''
    Var2 = ''
    Operation = ''
    def GetDisp(self):
        return self.dispVar.text

    def SetDisp(self, newtext):
        self.dispVar.text = str(newtext).rstrip(".0")

    def Add(self):
        self.Operation ='plus'
        self.Var1 = self.GetDisp()
        self.SetDisp('')
        print(self.Operation)

    def Subtract(self):
        self.Operation ='minus'
        self.Var1 = self.GetDisp()
        self.SetDisp('')
        print(self.Operation)

    def Multiply(self):
        self.Operation ='times'
        self.Var1 = self.GetDisp()
        self.SetDisp('')
        print(self.Operation)

    def Divide(self):
        self.Operation ='by'
        self.Var1 = self.GetDisp()
        self.SetDisp('')
        print(self.Operation)

    def SaveDispToVar1(self):
        self.Var1 = self.GetDisp()

    def Calculate(self):
        self.Var2 = self.GetDisp()
        print(self.Var1)
        print(self.Var2)
        if self.Operation == 'plus':
            self.SetDisp(float(self.Var1) + float(self.Var2))
        elif self.Operation == 'minus':
            self.SetDisp(float(self.Var1) - float(self.Var2))
        elif self.Operation == 'times':
            self.SetDisp(float(self.Var1) * float(self.Var2))
        elif self.Operation == 'by':
            self.SetDisp(float(self.Var1) / float(self.Var2))
        else:
            print('self.operation is not plus minus times nor by')
        self.Var1 = 0
        self.Var2 = 0
    def ClearAll(self):
        self.SetDisp('')
        self.Operation = ''
        self.Var1 = 0
        self.Var2 = 0



class MyScreenManager(ScreenManager):
    pass

if __name__== '__main__':
    CalcjpApp().run()
